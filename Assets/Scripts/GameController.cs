﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour
{

	public Camera cam;
	public GameObject[] balls;

	public float timeleft;
	public Text timer;
	public GameObject GameOver;
	public GameObject btnReplay;
	public GameObject splashScreen;
	public GameObject btnStart;
	public HatController hatCon;


	private float maxWidth;
	private bool counting = true;
	private bool playing;


	public void StartGame ()
	{
		splashScreen.SetActive (false);
		btnStart.SetActive (false);

		hatCon.ToggleControl (true);
		StartCoroutine (Spawn ());

	}

	public void Start ()
	{
		if (cam == null)
			cam = Camera.main;
		playing = false;
		//find the corner to set a maxwidth of the game
		Vector3 upperCorner = new Vector3 (Screen.width, Screen.height, 0f);
		Vector3 targetWidth = cam.ScreenToWorldPoint (upperCorner);
		float ballWidth = balls[0].GetComponent<Renderer> ().bounds.extents.x;
		maxWidth = targetWidth.x - ballWidth;

		timer.text = "Timer: " + Mathf.RoundToInt (timeleft);
		GameOver.SetActive (false);
		btnReplay.SetActive (false);
	}

	void FixedUpdate ()
	{
		if (playing) {
			if (counting) {
				timeleft -= Time.deltaTime;
				if (timeleft < 0)
					timeleft = 0;
				timer.text = "Timer: \n" + Mathf.RoundToInt (timeleft);
			}
		}
	}

	IEnumerator Spawn ()
	{
		yield return new WaitForSeconds (2f);
		playing = true;
		while (timeleft > 0) {
			//falling items
			GameObject ball = balls [Random.Range (0, balls.Length)];
			Vector3 spawnPos = new Vector3 (
				                   Random.Range (-maxWidth, maxWidth),
				                   transform.position.y,
				                   0f
			                   );
			Quaternion spawnRotation = Quaternion.identity;

			Instantiate (ball, spawnPos, spawnRotation);

			yield return new WaitForSeconds (Random.Range (1f, 2f));
		}
		yield return new WaitForSeconds (1f);
		GameOver.SetActive (true);
		yield return new WaitForSeconds (1f);
		btnReplay.SetActive (true);
	}

}
