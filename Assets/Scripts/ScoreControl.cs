﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreControl : MonoBehaviour {

	public Text scoreText;
	public int ballValue;
	public int score ;

	void Start(){
		score = 0;
		updateScore (); 	
	}
	void updateScore(){
		scoreText.text = "Score: \n" + score;
	}

	//check if Bomb
	void OnCollisionEnter2D (Collision2D collision) {
		if (collision.gameObject.tag == "Bomb") {
			score -= ballValue * 2;
			updateScore ();
		}
	}

	//get point
	void OnTriggerEnter2D(Collider2D a){		
			score += ballValue;
			updateScore ();

	}

}
