﻿using UnityEngine;
using System.Collections;

public class HatController : MonoBehaviour
{
	public Camera cam;

	private float maxWidth;
	private bool canControl;
	// Use this for initialization
	void Start ()
	{
		if (cam == null)
			cam = Camera.main;
		//find the corner to set a maxwidth of the game
		Vector3 upperCorner = new Vector3 (Screen.width, Screen.height, 0f);
		Vector3 targetWidth = cam.ScreenToWorldPoint (upperCorner);
		float hatWidth = GetComponent<Renderer> ().bounds.extents.x;
		maxWidth = targetWidth.x - hatWidth;
		canControl = false;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		//get mouse pos
		if (canControl) {
			Vector3 rawPos = cam.ScreenToWorldPoint (Input.mousePosition);
			Vector3 targetPos = new Vector3 (rawPos.x, 0f, 0f);
			//
			float tarWidth = Mathf.Clamp (targetPos.x, -maxWidth, maxWidth);
			targetPos = new Vector3 (tarWidth, targetPos.y, targetPos.z);
			//moving item 
			GetComponent<Rigidbody2D> ().MovePosition (targetPos);
		}
	}
	public void ToggleControl(bool check){
		canControl = check;
	}
}
